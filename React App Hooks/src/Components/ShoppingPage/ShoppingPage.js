import React from "react";
import ProductList from "../ProductList/ProductList";
import {useAPIInfo} from "../API_Conect/connectWithAPI";

export default function ShoppingPage() {
    return <ProductList getItem={useAPIInfo} type={'Bag'}/>
}