export const useAPIInfo = async () => {
    if(!localStorage.getItem('Favorit') || !localStorage.getItem('Bag')){
        localStorage.setItem('Favorit', JSON.stringify([]));
        localStorage.setItem('Bag', JSON.stringify([]))
    }

    const favoritProducts = JSON.parse(localStorage.getItem('Favorit'));
    const bagProducts = JSON.parse(localStorage.getItem('Bag'));

    return fetch('./data.json')
        .then(response => response.json())
        .then(items=> {
            return items.map(item => {
                return {
                    ...item,
                    inBag: bagProducts.some(i=> i.id === item.id),
                    inFav: favoritProducts.some(i => i.id === item.id)
                }
        })
    })
};
