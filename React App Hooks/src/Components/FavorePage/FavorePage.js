import React from "react";
import ProductList from "../ProductList/ProductList";
import {useAPIInfo} from "../API_Conect/connectWithAPI";

export default function FavorePage() {
    return <ProductList getItem={useAPIInfo} type={'Fav'}/>
}