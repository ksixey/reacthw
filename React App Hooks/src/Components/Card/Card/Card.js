import React from "react";
import classes from './Card.module.css'
import Star from "../../UI/Star/Star";
import {faStar, faStarHalfAlt} from "@fortawesome/free-solid-svg-icons";


export default function Card (props) {
    return(
            <div className={classes.item}>
                {props.inBag && props.type === 'Bag' ? <button onClick={props.toggleModal}  className={classes.deleteFromBag}>
                    &#10006;
                </button> : undefined}
                <div className={classes.photo}>
                    <img className={classes.imgItems} src={props.img} alt="article"/>
                </div>
                <div className={classes.info}>
                    <div className={classes.title}>
                    {props.title}
                    </div>
                    <div className={classes.article}>Article: {props.article}</div>
                    <div className={classes.cost}>{props.cost}</div>
                    <div className={classes.containerDif}>
                        {props.inBag ? 
                        <button  className={classes.card}>
                            Added to card
                        </button> 
                        : <button onClick={props.addProductToBag} className={classes.card}>Buy</button>}
                        <Star onClick={props.setProductToFav}
                            isFavore={props.inFav ? faStar : faStarHalfAlt}/>
                    </div>
                </div>
            </div>
        )
}