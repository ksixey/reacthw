import React, {useEffect, useState} from "react";
import Card from "../Card/Card/Card";
import Modal from "../UI/Modal/Modal"

export default function ProductList({type,getItem}){
    const [products, setProducts] = useState([]);
    const [currentProductID, setCurrentProductID] = useState(null);
    const [toggleModalState, setModal] = useState(false);
    const [modalDeleteFromBag, setModalDeleteFromBag] = useState(false);
    const [currentItem, setCurrentItem] = useState(null)
    
    useEffect(() => {
        (async () => {
            let productFromAPI = await getItem();
            if (type === 'Fav') {
                productFromAPI = productFromAPI.filter(product => product.inFav);
                console.log(productFromAPI);
                
            } else if(type === 'Bag') {
                productFromAPI = productFromAPI.filter(product => product.inBag)
            }
            setProducts(productFromAPI);
        })()
    }, [currentProductID,type,getItem]);

    const setProduct = (id,local,target) => {

        const currentProducts= [...products];
        const currentProduct = currentProducts.find(product => product.id === id);

        const localProducts = JSON.parse(localStorage.getItem(local));

        currentProduct[target] = !currentProduct[target];

        if(!localProducts.some(product => product.id === id)){
            localProducts.push(currentProduct)
        }else {
            const idx = localProducts.indexOf(localProducts.find(product => product.id === id));
            localProducts.splice(idx, 1);
    }

        setProducts(currentProducts);
        localStorage.setItem(local, JSON.stringify(localProducts))
    } 

    const setItemToLocal = (id, local, target) => {
        setCurrentProductID(id);
        setProduct(id, local, target)
    }

    const setProductToFav = (id) => {
        setItemToLocal(id, 'Favorit', 'inFav')
    }

    const addProductToBag = (id) => {
        toggleModal();
        setItemToLocal(id, 'Bag', 'inBag')
    }

    const deleteProductFromBag = (id) => {
        setModalDeleteFromBag(false);
        setItemToLocal(id, 'Bag', 'inBag')
    }

    const toggleModal = () => {
        setModal(!toggleModalState);
    }

    const handleClick = (e) => {
        e.stopPropagation()
    }
    
    const modalWorkWithBag = (item, bool) => {
        setModalDeleteFromBag(bool);
        setCurrentItem(item)
    }

    const currentProduct = products.filter(product => product.id === currentProductID)[0];
    
    return (
        <div className="list">
            {products.map(item => {
                return  <Card key={item.id}
                                    id={item.id}
                                    type={type}
                                    img={item.img}
                                    title={item.title}
                                    cost={item.cost}
                                    article={item.article}
                                    color={item.color}
                                    inBag={item.inBag}
                                    inFav = {item.inFav}
                                    addProductToBag={() => addProductToBag(item.id)}
                                    setProductToFav={() => setProductToFav(item.id)}
                                    toggleModal= {() => modalWorkWithBag(item, true)}
            />
            })}
        {toggleModalState && <Modal 
                                toggleModal={() => toggleModal()}
                                handleClick={(e)=> handleClick(e)}  
                                title={`You have added ${currentProduct.title} to the cart`}
                                />}
        
        {modalDeleteFromBag && <Modal
                                title= {`Are you sure you want to delete ${currentItem.title} ?`}
                                toggleModal= {() => setModalDeleteFromBag(false)}
                                modalDeleteFromBag={modalDeleteFromBag}
                                item = {currentItem}
                                deleteProductFromBag = {() => deleteProductFromBag(currentItem.id)}

        />

        }

        </div>
        )
}


