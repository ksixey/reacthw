import React from "react";
import PropTypes from 'prop-types'
import ButtonClose from "../ButtonClose/ButtonClose";
import classes from './Modal.module.css'
import Button from "../Button/Button";

const Modal =({toggleModal, handleClick, title, deleteProductFromBag, modalDeleteFromBag}) =>{
    return (
        <div className={classes.overlay} onClick={toggleModal}  >
            <div className={classes.window}  onClick={handleClick}>
                <div className={classes.container}>
                    <div className={classes.header}>
                        {title? <span>{title}</span> : undefined}
                        <ButtonClose toggleModal={toggleModal}/>
                    </div>
                    <div className={classes.footer}>
                    {modalDeleteFromBag && <Button onClick={deleteProductFromBag} text='OK'/>} 
                    <Button onClick={toggleModal} text='Cancel'/>
                    </div>
                </div>
            </div>
        </div>
    )
};

Modal.propTypes = {
    title: PropTypes.string,
    onClick: PropTypes.func,
    handleClick: PropTypes.func
};

export default Modal;