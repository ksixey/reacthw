import React from "react";
import Bag from "../../Header/Bag/Bag";
import {Link} from "react-router-dom";
import classes from './Menu.module.css'

export default function Menu() {
    return (
        <ul className={classes.menuUl}>
            <li className={classes.menuLi}>
                <Link className={classes.link} to={"/"}>
                    Main
                </Link>
            </li>
            <li className={classes.menuLi}>
                <Link className={classes.link} to={"/favorepage"}>
                    Favorite
                </Link>
            </li>
            <li className={classes.menuLi}>
                <Link className={classes.link} to={"/shoppingpage"}>
                    <Bag/>
                </Link>
            </li>
        </ul>
    )
}