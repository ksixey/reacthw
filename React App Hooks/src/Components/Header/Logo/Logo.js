import React, {Component} from "react";
import classes from './Logo.module.css'

export default class Logo extends Component {
    render() {
        return (
                <div className={classes.logo}>CandySHOP</div>
        )
    }
}