import React, {Component} from "react";
import classes from './Header.module.css'
import Logo from "./Logo/Logo";
import Menu from "../UI/Menu/Menu";


export default class Header extends Component {
    render() {
        return (
            <div className={classes.header}>
                <div className={"container"}>
                    <div className={classes.headerWrapper}>
                        <Logo/>
                        <Menu/>
                    </div>
                </div>
            </div>
        )
    }
}