import React, {useEffect} from 'react';
import './App.css';
import {Route, Switch} from 'react-router-dom';
import Header from "./Components/Header/Header";
import ShoppingPage from "./Components/ShoppingPage/ShoppingPage";
import FavorePage from "./Components/FavorePage/FavorePage";
import MainList from "./Components/MainList/MainList";

function App(){
    return (
        <div>
            <Header/>
            <div className="container">
                <Switch>
                    <Route path="/" exact component={MainList} />
                    <Route path="/shoppingpage"  component={ShoppingPage} />
                    <Route path="/favorepage" component={FavorePage} />
                </Switch>
            </div>
        </div>
    )
}

export default App;
