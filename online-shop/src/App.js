import React from 'react';
import './App.css';
import ProductList from "./Components/ProductList/ProductList";
import Header from "./Components/Header/Header";


export default class App extends React.Component {
    render() {
    return (
        <div>
            <Header/>
            <div className="container">
                <ProductList/>
            </div>
        </div>

    )
  }
};
