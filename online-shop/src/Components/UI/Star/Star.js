import React,{Component} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import classes from './Star.module.css'


export default class Star extends Component {
    render() {
        return (
            <FontAwesomeIcon onClick={this.props.onClick} onChange={this.props.onChange} icon={this.props.isFavore} className={classes.star} />
        )
    }
}