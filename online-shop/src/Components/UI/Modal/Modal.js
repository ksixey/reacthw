import React from "react";
import PropTypes from 'prop-types'
import ButtonClose from "../ButtonClose/ButtonClose";
import classes from './Modal.module.css'
import Button from "../Button/Button";

const Modal =({title,onClick, handleClick}) =>{
    return (
        <div className={classes.overlay} onClick={onClick}  >
            <div className={classes.window}  onClick={handleClick}>
                <div className={classes.container}>
                    <div className={classes.header}>
                        Hi, User!
                        <ButtonClose onClick={onClick}/>
                    </div>
                    <div className={classes.body}>
                        You put '{title}' in the card!
                    </div>
                    <div className={classes.footer}>
                       <Button onClick={onClick} text='OK'/>
                    </div>
                </div>
            </div>
        </div>
    )
};

Modal.propTypes = {
    title: PropTypes.string,
    onClick: PropTypes.func,
    handleClick: PropTypes.func
};

export default Modal;