import React, {Component} from "react";
import classes from './Header.module.css'
import Logo from "./Logo/Logo";
import Bag from "./Bag/Bag";

export default class Header extends Component {
    render() {
        return (
            <div className={classes.header}>
                <div className={"container"}>
                    <div className={classes.headerWrapper}>
                        <Logo/>
                        <Bag/>
                    </div>
                </div>
            </div>
        )
    }
}