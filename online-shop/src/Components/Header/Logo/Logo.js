import React, {Component} from "react";
import classes from './Logo.module.css'

export default class Logo extends Component {
    render() {
        return (
                <a className={classes.logo}>CandySHOP</a>
        )
    }
}