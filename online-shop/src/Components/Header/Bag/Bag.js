import React, {Component} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingBag} from "@fortawesome/free-solid-svg-icons";
import classes from './Bag.module.css'

export default class Bag extends Component {
    render() {
        return (
                <FontAwesomeIcon  className={classes.bag} icon={faShoppingBag}/>
        )
    }
}