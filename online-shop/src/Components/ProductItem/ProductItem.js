import React, {Component, Fragment} from "react";
import {getFavFromLocal,getBagFromLocal} from "../ProductList/ProductList";
import Card from "./Card/Card";
import Modal from "../UI/Modal/Modal";

export default class ProductItem extends Component {
    state = {
        openModal: false,
        buyItem: Array.isArray(getBagFromLocal()) ?
            JSON.parse(localStorage.getItem('Bag'))
                .find(item => item === this.props.id) : false,
        isFavore: Array.isArray(getFavFromLocal()) ?
            JSON.parse(localStorage.getItem('Favorit'))
                .find(item => item === this.props.id) : false,
    };

    clickToOpenModal = () => {
        if (!this.state.buyItem ) {
            this.setState({
                openModal: true,
            })
        }
    };
    clickToCloseModal = () => {
        this.addLocalStorageBag();
            this.setState({
                openModal: !this.state.openModal,
            });
    };

    addLocalStorageFav = () => {
        let card = this.props.id;
        this.setState({
            isFavore: !this.state.isFavore
        });

        if (this.state.isFavore) {
            localStorage.setItem('Favorit',
                JSON.stringify(getFavFromLocal().filter(item => item !== card)));
        } else {
            if (Array.isArray(getFavFromLocal())) {
                localStorage.setItem('Favorit',
                    JSON.stringify([...getFavFromLocal(), card]))
            } else {
                localStorage.setItem('Favorit',
                    JSON.stringify([card]))
            }
        }
    };
    addLocalStorageBag = () => {
        let card = this.props.id;
        this.setState({
            buyItem: true
        });
        if (this.state.buyItem) {
            localStorage.setItem('Bag', JSON.stringify(getBagFromLocal().filter(item => item !== card)))
        } else {
            if (Array.isArray(getBagFromLocal())) {
                localStorage.setItem('Bag',
                    JSON.stringify([...getBagFromLocal(), card]))
            } else {
                localStorage.setItem('Bag',
                    JSON.stringify([card]))
            }

        }
    };

    handleClick(e) {
        e.stopPropagation()
    }

    render() {
        return (
            <Fragment>
                <Card title={this.props.title}
                      img={this.props.img}
                      cost={this.props.cost}
                      article={this.props.article}
                      buyItem={this.state.buyItem}
                      isFavore={this.state.isFavore}
                      openModal={this.clickToOpenModal}
                      addToFavore={this.addLocalStorageFav}
                />
                {this.state.openModal &&
                <Modal title={this.props.title}
                       onClick={this.clickToCloseModal}
                       handleClick = {this.handleClick}
                />}
            </Fragment>
        )
    }
}