import React,{Component} from "react";
import classes from './Card.module.css'
import Star from "../../UI/Star/Star";
import {faStar, faStarHalfAlt} from "@fortawesome/free-solid-svg-icons";

export default class Card extends Component {
    render() {
        let {title,img,cost,article} =this.props;
        return(
            <div className={classes.item}>
                <div className={classes.photo}>
                    <img className={classes.imgItems} src={img} alt="article"/>
                </div>
                <div className={classes.info}>
                    <div className={classes.title}>
                      {title}
                    </div>
                    <div className={classes.article}>Article: {article}</div>
                    <div className={classes.cost}>{cost}</div>
                    <div className={classes.containerDif}>
                        <button className={classes.card}
                                onClick={this.props.openModal}>{ this.props.buyItem  ? 'Added to card' : 'Buy' }</button>
                        <Star onClick={this.props.addToFavore}
                              isFavore={this.props.isFavore ? faStar : faStarHalfAlt}/>
                    </div>
                </div>
            </div>
        )
    }
}