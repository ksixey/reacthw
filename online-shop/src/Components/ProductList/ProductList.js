import React, {Component} from "react";
import ProductItem from "../ProductItem/ProductItem";
import classes from './ProductList.module.css';
import Modal from "../UI/Modal/Modal";

export const getFavFromLocal = () => JSON.parse(localStorage.getItem('Favorit'));
export const getBagFromLocal = () =>  JSON.parse(localStorage.getItem('Bag'));

export default class ProductList extends Component {
    state = {
        data: [],
    };


    componentDidMount() {
        fetch('./data.json')
            .then(res=>res.json())
            .then(items =>{
                this.setState({
                        data: items
                    })
                }
            )
    }

    render() {
        const {data} = this.state;
        return (
            <div className={classes.list}>
                {data.map((data) => {
                    return <ProductItem key={data.id}
                                        id={data.id}
                                        title={data.title}
                                        img={data.img}
                                        cost={data.cost}
                                        article={data.article}
                                        color={data.color}
                    />
                })}

            </div>
        )
    }
}

